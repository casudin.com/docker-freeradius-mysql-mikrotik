#!/bin/bash
set -ev
docker-compose up -d
docker pull 2stacks/radtest
# Wait for MySQL to bootstrap
sleep 15
docker-compose ps
#radtest username password radius_server_secret nas_ip_address nas_port_number
#radtest john secret 127.0.0.1:1812 0 testing123

docker run -it --rm --network docker-freeradius_backend 2stacks/radtest radtest testing password my-radius 1812 testing123
#docker run -it --rm --network docker-freeradius_backend 2stacks/radtest radtest testing password my-radius 172.21.0.2 8728 testing123 
#docker exec -it docker-radius-mysql_radius_1 radtest fredf wilma localhost testing123

# docker exec -it stingy_exchange radtest testing password localhost testing123
# radtest user1 password1 172.28.0.3 0 testing123 nasipaddress=192.168.1.100
# radtest hotspotuser password1123 172.28.5.3:1812 8728 testing123  0 172.28.0.11
# radtest user-nas-1 password123 172.28.0.3:1812 8728 testing123  0 172.28.0.4
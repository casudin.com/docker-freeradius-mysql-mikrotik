#!/bin/sh

# Start OpenVPN
openvpn --config /etc/openvpn/server.conf &

# Wait until OpenVPN is ready
while ! nc -z localhost 1194; do
  sleep 1
done

# Start FreeRADIUS
freeradius -f

